import 'package:flutter/material.dart';

void main() => runApp(MarketScreen());

class MarketScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Column(children: [
            Text('Bambu Pazarına Hoş geldin!'),
            Text('burada bambu-coinlerini \nBambularla takas edebilirsin '),
            ElevatedButton.icon(
              onPressed: () {
                Navigator.of(context).pushNamed(
                  '/win',
                );
              },
              icon: Icon(Icons.monetization_on_outlined),
              label: Text('Takas Et'),
            )
          ]),
        ),
      ),
    );
  }
}
