import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/material.dart';

class StartScreen extends StatelessWidget {
  static AudioCache player = AudioCache(prefix: 'assets/audio/');
  @override
  Widget build(BuildContext context) {
    player.play('app_start.mp3');
    return Material(
      child: Container(
        padding: EdgeInsets.all(4.0),
        color: Colors.deepOrange.shade50,
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
              'assets/images/start/bambu.png',
              width: 120,
              semanticLabel: "bambu logo",
            ),
            Image.asset(
              "assets/images/start/rolly1.png",
              width: 320,
              semanticLabel: "Rolly",
            ),
            Padding(
              padding: EdgeInsets.all(6.0),
              child: Text(
                'Panda Rolly',
                style: TextStyle(
                    color: Colors.green.shade900,
                    fontSize: 33.0,
                    fontFamily: "DiceGameDemo"),
                textAlign: TextAlign.center,
              ),
            ),
            OutlinedButton(
              child: Text(
                'ile Maceraya...',
                style: TextStyle(fontFamily: "MALDINI", fontSize: 22),
              ),
              onPressed: () {
                Navigator.of(context).pushNamed(
                  '/rules',
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
