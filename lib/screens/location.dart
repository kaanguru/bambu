import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:bambu/core/random_generator.dart';
import 'package:bambu/data/api/models/game_location.dart';
import 'package:bambu/business/CoinBar.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocationScreen extends StatefulWidget {
  final GameLocation location;
  LocationScreen({
    @required this.location,
  });

  static AudioCache storyPlayer = AudioCache(prefix: 'assets/audio/locations/');
  static AudioPlayer audioPlayer = AudioPlayer();
  @override
  _LocationScreenState createState() => _LocationScreenState();
}

class _LocationScreenState extends State<LocationScreen> {
  int calculatedCoin = 0;
  bool _isPlaying = false;
  @override
  void initState() {
    setLocationAsVisited(widget.location.id);
    calculatedCoin = calculateCoin(widget.location.pazaraM);
    addAwardToCoin(calculatedCoin);

    Fluttertoast.showToast(
        msg: "Tam " + calculatedCoin.toString() + " Bambu Coin Kazandın !",
        // toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.green.shade900,
        textColor: Colors.white,
        fontSize: 16.0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final locationGif = Image.asset(
      "assets/images/locations/" + widget.location.id.toString() + ".gif",
      width: MediaQuery.of(context).size.width - 50,
      semanticLabel: widget.location.isim,
    );
    return Material(
      child: Container(
        color: Colors.deepOrange.shade50,
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CoinBar(
              awardCoin: calculatedCoin,
            ),
            Text('Yeni Konum Bulundu'),
            Text(
              widget.location.isim,
              style: TextStyle(
                color: Colors.green.shade900,
                fontSize: 33.0,
                fontFamily: "Stick",
              ),
              textAlign: TextAlign.center,
            ),
            locationGif,
            IconButton(
              icon: Icon(_isPlaying
                  ? Icons.pause_circle_filled_rounded
                  : Icons.play_circle_fill_rounded),
              iconSize: 66,
              color: Colors.green,
              tooltip: 'Hikaye Dinle',
              onPressed: () {
                setState(() {
                  _isPlaying = !_isPlaying;
                });
                if (!_isPlaying) {
                  LocationScreen.storyPlayer
                      .play(widget.location.id.toString() + '.mp3');
                }
              },
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pushNamed(
                  '/rules',
                );
              },
              child: Text('Yeni Maceraya'),
            )
          ],
        ),
      ),
    );
  }
}

int calculateCoin(int distance) {
  int closestLocationsDistance = 95;
  return (distance > 0)
      ? getRandomInt((closestLocationsDistance ~/ 2) - 1, (distance ~/ 2))
      : 0;
}

setLocationAsVisited(int visitedID) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setInt('lastID', (visitedID));
}
