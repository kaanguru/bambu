import 'package:flutter/material.dart';
import 'dart:math';
import 'package:confetti/confetti.dart';
import 'package:bambu/business/CoinBar.dart';

void main() => runApp(WinBambooScreen());

class WinBambooScreen extends StatefulWidget {
  @override
  _WinBambooScreenState createState() => _WinBambooScreenState();
}

class _WinBambooScreenState extends State<WinBambooScreen> {
  Future<int> _bambooPieces = readCoins().then((value) => value ~/ 10);
  ConfettiController _controllerTopCenter =
      ConfettiController(duration: const Duration(seconds: 1));

  @override
  void dispose() {
    _controllerTopCenter.dispose();
    super.dispose();
  }

  /// A custom Path to paint stars.
  Path drawStar(Size size) {
    // Method to convert degree to radians
    double degToRad(double deg) => deg * (pi / 180.0);

    const numberOfPoints = 5;
    final halfWidth = size.width / 2;
    final externalRadius = halfWidth;
    final internalRadius = halfWidth / 2.5;
    final degreesPerStep = degToRad(360 / numberOfPoints);
    final halfDegreesPerStep = degreesPerStep / 2;
    final path = Path();
    final fullAngle = degToRad(360);
    path.moveTo(size.width, halfWidth);

    for (double step = 0; step < fullAngle; step += degreesPerStep) {
      path.lineTo(halfWidth + externalRadius * cos(step),
          halfWidth + externalRadius * sin(step));
      path.lineTo(halfWidth + internalRadius * cos(step + halfDegreesPerStep),
          halfWidth + internalRadius * sin(step + halfDegreesPerStep));
    }
    path.close();
    return path;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: FutureBuilder(
          future: _bambooPieces,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                itemCount: snapshot.data,
                itemBuilder: (BuildContext context, int index) {
                  return Row(
                    children: [
                      IconButton(
                          icon: Image.asset(
                            "assets/images/tek-bambu.png",
                          ),
                          iconSize:
                              MediaQuery.of(context).size.shortestSide / 2,
                          onPressed: () => eat()),
                      ConfettiWidget(
                        confettiController: _controllerTopCenter,
                        blastDirectionality: BlastDirectionality
                            .explosive, // don't specify a direction, blast randomly
                        shouldLoop:
                            false, // start again as soon as the animation is finished
                        colors: const [
                          Colors.green,
                          Colors.blue,
                          Colors.pink,
                          Colors.orange,
                          Colors.purple
                        ], // manually specify the colors to be used
                        createParticlePath:
                            drawStar, // define a custom shape/path.
                      )
                    ],
                  );
                },
              );
            } else if (snapshot.hasError) {
              return Text("Henüz Koin yok. BambuCoin Topla");
            } else {
              return Text("data hasError");
            }
          }),
    );
  }

  void eat() {
    _controllerTopCenter.play();
    setState(() {
      _bambooPieces = _bambooPieces.then((value) => value - 1);
    });
  }
}
