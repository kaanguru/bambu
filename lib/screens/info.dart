import 'package:flutter/material.dart';

void main() => runApp(InfoScreen());

class InfoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        color: Colors.green.shade900,
        child: Image.asset("assets/images/kardelen-logo.png"),
      ),
    );
  }
}
