import 'package:flutter/material.dart';
import 'package:extended_image/extended_image.dart';

void main() => runApp(MapScreen());

class MapScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).copyWith().size.height,
      color: Colors.deepOrange.shade50,
      alignment: Alignment.center,
      child: ExtendedImage.network(
        "https://priceless-thompson-a99a1a.netlify.app/_nuxt/img/map.4f0eb6d.png",
        fit: BoxFit.contain,
        mode: ExtendedImageMode.gesture,
        initGestureConfigHandler: (state) {
          return GestureConfig();
        },
      ),
    );
  }
}
