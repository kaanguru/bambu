import 'dart:async';
import 'package:bambu/business/CoinBar.dart';
import 'package:bambu/business/location_check.dart';
import 'package:bambu/business/search_in_game_locations.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class RulesScreen extends StatefulWidget {
  @override
  _RulesScreenState createState() => _RulesScreenState();
}

class _RulesScreenState extends State<RulesScreen> {
  StreamSubscription subscription;
  // kurallar sayfasına girildiğinde ilk çalıştırılacaklar
  @override
  void initState() {
    super.initState();
    // üyelik başlat
    subscription = Geolocator.getPositionStream(
            desiredAccuracy: LocationAccuracy.high, distanceFilter: 6)
        // CİHAZ BOYLAMI OYUN BOYLAMLARI İÇİNDEYSE
        .where(
            (Position devPos) => isInGameLocationLongitudes(devPos.longitude))
        .listen((Position devPosition) {
      // OYUN KONUMLARI İÇİNDE ARAMA YAP eğer Doğru konum bulursan
      // konum sayfasına veya pazara gönder
      searchInGameLocationsThenGoToRelatedPage(devPosition, context,
          sensitivity: 4);
    });
  }

  @override
  void dispose() {
    // cancel your subscription when the class is removed
    subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        color: Colors.deepOrange.shade50,
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CoinBar(
              awardCoin: 0,
            ),
            Text(
              'Panda Rolly',
              style: TextStyle(
                color: Colors.green.shade900,
                fontSize: 28.0,
                fontFamily: "DiceGameDemo",
                letterSpacing: 5,
              ),
              textAlign: TextAlign.center,
            ),
            Text(
              'Acıktı',
              style: TextStyle(
                  color: Colors.lightGreen.shade900,
                  fontSize: 22.0,
                  fontFamily: "Stick"),
              textAlign: TextAlign.center,
            ),
            imageWithText(context),
            Padding(
              padding: const EdgeInsets.all(2.0),
              child: Text(
                'Çevrendeki herhangi bir\nBambuCoin kaynağına doğru\nYola Çık',
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Stack imageWithText(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.topCenter,
      children: [
        Container(
          padding: const EdgeInsets.all(2.0),
          height: MediaQuery.of(context).size.height * 0.5,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                "assets/images/bambulu-yol.png",
              ),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              decoration: BoxDecoration(
                color: Colors.white70.withOpacity(0.8),
                border: Border.all(color: Colors.green.shade900, width: 1),
              ),
              child: Text(
                'Bambu alması için\nBambuCoin toplamasına\nYardım eder misin?',
                style: TextStyle(color: Colors.black, fontSize: 16.0),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.25),
            Text(
              'Ekranı Kapat!',
              style: Theme.of(context).textTheme.headline5,
              textAlign: TextAlign.center,
            ),
            Text(
              'Hareket Et!',
              style: Theme.of(context).textTheme.headline4,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ],
    );
  }
}
