import 'package:bambu/screens/info.dart';
import 'package:bambu/screens/location.dart';
import 'package:bambu/screens/map.dart';
import 'package:bambu/screens/market.dart';
import 'package:bambu/screens/win_bamboo_screen.dart';
import 'package:flutter/material.dart';
import 'package:bambu/screens/start.dart';
import 'package:bambu/screens/rules.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => StartScreen());
      case '/rules':
        return MaterialPageRoute(
          builder: (_) => RulesScreen(),
        );
      case '/location':
        return MaterialPageRoute(
            builder: (_) => LocationScreen(location: args));
      case '/market':
        return MaterialPageRoute(builder: (_) => MarketScreen());
      case '/win':
        return MaterialPageRoute(builder: (_) => WinBambooScreen());
      case '/info':
        return MaterialPageRoute(builder: (_) => InfoScreen());
      case '/map':
        return MaterialPageRoute(builder: (_) => MapScreen());

      default:
        // print("olmayan sayfa");
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('HATA'),
        ),
        body: Center(
          child: Text('ROTA HATA'),
        ),
      );
    });
  }
}
