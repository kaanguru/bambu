import 'dart:math';

int getRandomInt(int min, int max) {
  Random rnd = new Random();
  return min + rnd.nextInt(max - min);
}
