import 'dart:math';

double roundToDecimals(double numToRound, int deciPlaces) {
  double modPlus1 = pow(10.0, deciPlaces + 1);
  String strMP1 = ((numToRound * modPlus1).roundToDouble() / modPlus1)
      .toStringAsFixed(deciPlaces + 1);
  int lastDigitStrMP1 = int.parse(strMP1.substring(strMP1.length - 1));

  double mod = pow(10.0, deciPlaces);
  String strDblValRound =
      ((numToRound * mod).roundToDouble() / mod).toStringAsFixed(deciPlaces);
  int lastDigitStrDVR =
      int.parse(strDblValRound.substring(strDblValRound.length - 1));

  return (lastDigitStrMP1 == 5 && lastDigitStrDVR % 2 != 0)
      ? ((numToRound * mod).truncateToDouble() / mod)
      : double.parse(strDblValRound);
}
