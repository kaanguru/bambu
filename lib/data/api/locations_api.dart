import 'dart:convert';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import 'models/game_location.dart';

Future<List<GameLocation>> getGameLocations() async {
  final url = 'https://api.jsonbin.io/v3/b/606b477a8be464182c592d16';
  final jsonFile = await DefaultCacheManager().getSingleFile(url);
  final jsonAsString = await jsonFile.readAsString();
  final body = await json.decode(jsonAsString);
  return GameLocations.fromJson(body).record;
}
