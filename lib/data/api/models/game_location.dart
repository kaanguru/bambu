class GameLocations {
  List<GameLocation> record;

  GameLocations({this.record});

  GameLocations.fromJson(Map<String, dynamic> json) {
    if (json['record'] != null) {
      record = <GameLocation>[];
      json['record'].forEach((v) {
        record.add(GameLocation.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (record != null) {
      data['record'] = record.map((v) => v.toJson()).toList();
    }

    return data;
  }
}

class GameLocation {
  int id;
  String isim;
  String tarif;
  String ipucu;
  double enlem;
  double boylam;
  int pazaraM;

  GameLocation(
      {this.id,
      this.isim,
      this.tarif,
      this.ipucu,
      this.enlem,
      this.boylam,
      this.pazaraM});

  GameLocation.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    isim = json['isim'];
    tarif = json['tarif'];
    ipucu = json['ipucu'];
    enlem = json['enlem'];
    boylam = json['boylam'];
    pazaraM = json['pazara_m'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['isim'] = isim;
    data['tarif'] = tarif;
    data['ipucu'] = ipucu;
    data['enlem'] = enlem;
    data['boylam'] = boylam;
    data['pazara_m'] = pazaraM;
    return data;
  }
}
