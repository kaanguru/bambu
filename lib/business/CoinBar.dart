import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CoinBar extends StatefulWidget {
  final int awardCoin;
  const CoinBar({Key key, this.awardCoin}) : super(key: key);
  @override
  _CoinBarState createState() => _CoinBarState();
}

class _CoinBarState extends State<CoinBar> {
  int currentCoin = 0;

  @override
  build(BuildContext context) {
    updateCurrentCoin() async {
      final _currentCoinOnSP = await readCoins();
      setState(() {
        currentCoin = _currentCoinOnSP;
      });
      // print(currentCoin);
    }

    return AppBar(
      leading: IconButton(
        icon: Image.asset("assets/images/bambuCoin.png"),
        tooltip: 'Coin',
        padding: EdgeInsets.only(left: 6),
        iconSize: 66,
        onPressed: () => {updateCurrentCoin()},
      ),
      title: displayCoin(currentCoin),
      actions: [
        IconButton(
          icon: Icon(Icons.map),
          tooltip: 'Harita',
          onPressed: () => {
            Navigator.of(context).pushNamed(
              '/map',
            )
          },
        ),
      ],
    );
  }
}

Future<int> readCoins() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  int coin = prefs.getInt('coin');
  return coin;
}

addAwardToCoin(int award) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  int _existingCoins = await readCoins();
  await prefs.setInt('coin', ((_existingCoins ?? 0) + award));
}

displayCoin(currentCoin) {
  return (currentCoin == 0 || currentCoin == null)
      ? Icon(Icons.visibility_off)
      : Text(currentCoin.toString());
}
