import 'package:bambu/core/round_doubles.dart';
import 'package:bambu/data/api/locations_api.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

import 'location_check.dart';

void searchInGameLocationsThenGoToRelatedPage(
    Position devPosition, BuildContext context,
    {int sensitivity}) {
  double devLatRounded = roundToDecimals(devPosition.latitude, sensitivity);
  double devLonRounded = roundToDecimals(devPosition.longitude, sensitivity);

  getGameLocations().then((gLocations) => {
        gLocations.forEach((location) {
          double locLatRounded = roundToDecimals(location.enlem, sensitivity);
          double locLonRounded = roundToDecimals(location.boylam, sensitivity);
          (locLatRounded == devLatRounded && locLonRounded == devLonRounded)
              ? lastLocation().then((lastId) => {
                    (lastId != location.id)
                        ? ((location.id == 0)
                            ? gotoMarket(context)
                            : goToLocation(location, context))
                        : false
                  })
              // ignore: unnecessary_statements
              : false;
        })
      });
}
