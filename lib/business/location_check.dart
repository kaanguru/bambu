import 'package:bambu/core/round_doubles.dart';
import 'package:bambu/data/api/models/game_location.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

bool isInGameLocationLongitudes(double longitude) {
  List gameLongitudes = [
    29.268,
    29.262,
    29.2644,
    29.2656,
    29.2675,
    29.2702,
    29.2666,
  ];
  return gameLongitudes.contains(roundToDecimals(longitude, 4));
}

Future<int> lastLocation() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getInt('lastID') ?? -1;
}

void goToLocation(GameLocation location, context) {
  // print("bulunan yer: ${location.isim}");
  Navigator.pushReplacementNamed(
    context,
    '/location',
    arguments: location,
  );
}

gotoMarket(context) {
  Navigator.pushReplacementNamed(
    context,
    '/market',
  );
}
