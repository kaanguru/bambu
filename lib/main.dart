import 'package:flutter/material.dart';
import 'package:bambu/core/routes_generator.dart';

void main() {
  runApp(BambooApp());
}

class BambooApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Bambu',
      theme: ThemeData(primarySwatch: Colors.green, fontFamily: "Stick"),
      initialRoute: '/',
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}
